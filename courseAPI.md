# Course RESTful API application

A [Sails](http://sailsjs.org) application

# Simple API Document

> Pre-caution: Please read the document perfectly. If you have any question, please don't hesitate to chat with me. Thank you !

| Type | Path | Description |
| ---- | ---- | ----------- |
| GET  | /course | This path generates all course(s) |
| GET  | /course/{id} | This path will generate course details by id you provides. |


---
`[GET] /course`

This path generates all course(s).

---
`[GET] /course/{id}`

This path will generate course details by id you provides.

---
(Will be more update...)